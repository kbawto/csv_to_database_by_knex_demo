import fs from 'fs'

const filePath = 'user.csv'
const users = []
const userHeader = 'username,password'

for (let i = 0; i < 100; i++) {
    users.push({
        username: `user${i}`,
        password: '123456'
    })
}

fs.writeFileSync(filePath, userHeader, 'utf-8')

for (const user of users) {
    const { username, password } = user
    let csvData = `\n${username},${password}`
    fs.writeFileSync(filePath, csvData, {
        flag: 'a+',
        encoding: 'utf-8'
    })
}