import { Knex } from "knex";
import fs from 'fs'
import { csvToObjectList } from '../util/csvToObjectList'

interface IUser {
    username: string,
    password: string
}

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("users").del();

    // Inserts seed entries
    const userCsvPath = 'user.csv'
    const data = fs.readFileSync(userCsvPath, 'utf-8')
    const users = csvToObjectList<IUser>(data)

    await knex("users").insert(users);
};
