## csv to database by knex demo

1. create typescript project

- install typescript

```bash
yarn init -y
yarn add typescript ts-node @types/node
```

- create tsconfig.json

```json
{
  "compilerOptions": {
    "module": "commonjs",
    "target": "es5",
    "lib": ["es6", "dom"],
    "sourceMap": true,
    "allowJs": true,
    "jsx": "react",
    "esModuleInterop": true,
    "moduleResolution": "node",
    "noImplicitReturns": true,
    "noImplicitThis": true,
    "noImplicitAny": true,
    "strictNullChecks": true,
    "suppressImplicitAnyIndexErrors": true,
    "noUnusedLocals": true
  },
  "exclude": ["node_modules", "build", "scripts", "index.js"]
}
```

2. Create csv

- Create createCsv.ts

```typescript
import fs from "fs";

const filePath = "user.csv";
const users = [];
const userHeader = "name,password";

for (let i = 0; i < 100; i++) {
  users.push({
    name: `user${i}`,
    password: "123456",
  });
}

fs.writeFileSync(filePath, userHeader, "utf-8");

for (const user of users) {
  const { name, password } = user;
  let csvData = `\n${name},${password}`;
  fs.writeFileSync(filePath, csvData, {
    flag: "a+",
    encoding: "utf-8",
  });
}
```

3. Create database

```sql
CREATE DATABASE demo;
CREATE TABLE users (
	id serial PRIMARY KEY,
	username VARCHAR ( 50 ) UNIQUE NOT NULL,
	password VARCHAR ( 50 ) NOT NULL
);
```

4. Insert csv data to database

- Install knex

```bash
yarn add knex pg @types/pg
yarn knex init -x ts
```

- create .env

```.env
DB_NAME=demo
DB_USERNAME=
DB_PASSWORD=
```

- Config knex

```typescript
import dotenv from "dotenv";
dotenv.config();
// Update with your config settings.

module.exports = {
  development: {
    client: "postgresql",
    connection: {
      database: process.env.DB_NAME,
      user: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: "knex_migrations",
    },
  },
  //......
};
```

- create knex seed

```bash
yarn knex seed:make 01-csv_users
```

- read csv in seed

```typescript
interface IUser {
  username: string;
  password: string;
}

const userCsvPath = "user.csv";
const data = fs.readFileSync(userCsvPath, "utf-8").split("\n");
data.shift();
const users: IUser[] = data.map((csv) => {
  const user = csv.split(",");
  const userObj: IUser = {
    name: user[0],
    password: user[1],
  };
  return userObj;
});
```

- Insert into database by knex

```typescript
await knex("users").insert(users);
```

## For all csv

- Create csvToObject.ts (Please make sure csv herder is same with database column name when you use this function !!!)

```typescript
export function csvToObjectList<T>(csvData: string): T[] {
  const dataList = csvData.split("\n");
  const header = (dataList.shift() as string).split(",");
  const csvObjectList: T[] = [];
  for (const data of dataList) {
    const obj = {} as T;
    for (let i = 0; i < header.length; i++) {
      obj[header[i]] = data.split(",")[i];
    }
    csvObjectList.push(obj);
  }
  return csvObjectList;
}
```

- Use In seed file

```typescript
import { Knex } from "knex";
import fs from "fs";
import { csvToObjectList } from "../util/csvToObjectList";

interface IUser {
  username: string;
  password: string;
}

export async function seed(knex: Knex): Promise<void> {
  // Deletes ALL existing entries
  await knex("users").del();

  // Inserts seed entries
  const userCsvPath = "user.csv";
  const data = fs.readFileSync(userCsvPath, "utf-8");
  const users = csvToObjectList<IUser>(data);

  await knex("users").insert(users);
}
```
