export function csvToObjectList<T>(csvData: string): T[] {
    const dataList = csvData.split('\n')
    const header = (dataList.shift() as string).split(',')
    const csvObjectList: T[] = []
    for (const data of dataList) {
        const obj = {} as T
        for (let i = 0; i < header.length; i++) {
            obj[header[i]] = data.split(',')[i]
        }
        csvObjectList.push(obj)
    }
    return csvObjectList
}